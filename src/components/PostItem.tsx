import React, {FC} from 'react';
import {IPost} from "../models/IPost";

interface PostItemProps {
    post: IPost;
}

const PostItem: FC<PostItemProps> = ({post}) => {
    return (
        <div style={{border: 'solid 1px orange', padding: 10, margin: '5px 0 5px 0', width: 200}}>
            {post.id}. {post.title}
            <button style={{margin: '0 0 0 10px'}}>Удалить</button>
        </div>
    );
};

export default PostItem;