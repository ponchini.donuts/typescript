import React from 'react';
import {postAPI} from "../services/PostService";
import PostItem from "./PostItem";
import {IPost} from "../models/IPost";

const PostContainer = () => {
    const {data: posts, isLoading, error} = postAPI.useFetchAllPostsQuery(100)
    const [createPost, {}] = postAPI.useCreatePostMutation()

    const handleCreate = async () => {
        const title = prompt()
        await createPost({title, body: title} as IPost)
    }

    return (
        <div>
            <div>
                <button onClick={handleCreate}>Добавить пост</button>
                {isLoading && <h1>Идет загрузка...</h1>}
                {error && <h1>Ошибка загрузки</h1>}
                {posts && posts.map(post =>
                    <PostItem post={post}/>
                )}
            </div>
        </div>
    );
};

export default PostContainer;